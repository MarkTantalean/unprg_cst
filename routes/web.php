<?php

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest')->name('principal');

Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/registrar', 'Auth\RegisterController@showRegistrationForm')->name('registrar');
Route::post('/registrar', 'Auth\RegisterController@register');

Route::group([
    'namespace'  => 'Admin',
    'prefix'     => 'administrativo',
    'middleware' => ['auth', 'acceso']
], function () {
    Route::get('/tesis/pendientes', 'TesisController@gestionarTesisPendientes')->name('admin.tesis.pendientes');
    Route::get('/tesis/aceptadas', 'TesisController@consultarTesisAceptadas')->name('admin.tesis.aceptadas');
    Route::get('/tesis/rechazadas', 'TesisController@consultarTesisRechazadas')->name('admin.tesis.rechazadas');
    Route::get('tesis/proceso/{id}', 'ProcesoController@revisar')->name('admin.proceso.revisar');
    Route::post('tesis/proceso/asesor', 'ProcesoController@responderSolicitudAsesor')->name('admin.proceso.asesor');
    Route::post('tesis/proceso/jurado', 'ProcesoController@responderSolicitudJurado')->name('admin.proceso.jurado');
    Route::post('tesis/proceso/oficializacion', 'ProcesoController@responderSolicitudOficializacion')->name('admin.proceso.oficializacion');
    Route::post('tesis/proceso/sustentacion', 'ProcesoController@responderSolicitudSustentacion')->name('admin.proceso.sustentacion');
});

Route::group([
    'namespace'  => 'Alumno',
    'prefix'     => 'alumno',
    'middleware' => ['auth', 'acceso']
], function () {
    Route::get('/tesis/gestionar', 'TesisController@gestionar')->name('alumno.tesis.gestionar');
    Route::get('/tesis/consultar', 'TesisController@consultar')->name('alumno.tesis.consultar');
    Route::post('/tesis/asesor', 'TesisController@solicitarAsesor')->name('alumno.tesis.asesor');
    Route::post('/tesis/jurado', 'TesisController@solicitarJurado')->name('alumno.tesis.jurado');
    Route::post('/tesis/oficializacion', 'TesisController@solicitarOficializacion')->name('alumno.tesis.oficializacion');
    Route::post('/tesis/sustentacion', 'TesisController@solicitarSustentacion')->name('alumno.tesis.sustentacion');

});

