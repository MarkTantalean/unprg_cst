@if (Auth::user()->tipo_usuario == 'S')
<li class="has-submenu">
    <a href="#"><i class="icon-home"></i>Inicio</a>
</li>

<li class="has-submenu">
    <a href="#"><i class="icon-folder"></i>Control Tesis</a>
    <ul class="submenu">
        <li>
            <ul>
                <li><a href="{{ route('admin.tesis.pendientes') }}">Procesar Solicitudes</a></li>
                <li><a href="{{ route('admin.tesis.aceptadas') }}">Solicitudes Aceptadas</a></li>
                <li><a href="{{ route('admin.tesis.rechazadas') }}">Solicitudes Rechazadas</a></li>
            </ul>
        </li>
    </ul>
</li>
@else
{{-- Menu para el alumno --}}
<li class="has-submenu">
    <a href="#"><i class="icon-home"></i>Inicio</a>
</li>

<li class="has-submenu">
    <a href="#"><i class="icon-folder"></i>Tesis</a>
    <ul class="submenu">
        <li>
            <ul>
                <li><a href="{{ route('alumno.tesis.gestionar') }}">Gestionar</a></li>
                <li><a href="{{ route('alumno.tesis.consultar') }}">Consultar</a></li>
            </ul>
        </li>
    </ul>
</li>
@endif
