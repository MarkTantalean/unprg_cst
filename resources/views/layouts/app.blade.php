<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <title>Control de Seguimiento de Tesis</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    @yield('plugins-styles')

    <!-- App css -->
    <link href="{{ asset('highdmin/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('highdmin/css/icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('highdmin/css/style.css') }}" rel="stylesheet" />

    @yield('styles')
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <script src="{{ asset('highdmin/js/modernizr.min.js') }}"></script>
</head>

<body>

    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container-fluid">

                <div class="logo">
                    <a href="index.html" class="logo">
                        <span class="logo-small"><i class="mdi mdi-radar"></i> CST</span>
                        <span class="logo-large"><i class="mdi mdi-radar"></i> Control de Seguimiento de Tesis</span>
                    </a>
                </div>

                <div class="menu-extras topbar-custom">

                    <ul class="list-unstyled topbar-right-menu float-right mb-0">

                        <li class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle nav-link">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>

                        <li class="dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                                aria-expanded="false">
                                    <img src="{{ asset('highdmin/images/users/avatar-1.jpg') }}" alt="user" class="rounded-circle"> <span class="ml-1 pro-user-name">
                                       {{ Auth::user()->usuario->persona->nombreCompleto() }} <i class="mdi mdi-chevron-down"></i> </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fi-head"></i> <span>Mi Cuenta</span>
                                </a>

                                <a href="javascript:void(0);" class="dropdown-item notify-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fi-power"></i> <span>Cerrar Sesión</span>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- end menu-extras -->

                <div class="clearfix"></div>

            </div>
            <!-- end container -->
        </div>
        <!-- end topbar-main -->

        <div class="navbar-custom">
            <div class="container-fluid">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu">

                        @include('layouts.opciones-menu')

                        {{-- <li class="has-submenu">
                            <a href="#"><i class="icon-layers"></i>Apps</a>
                            <ul class="submenu">
                                <li><a href="apps-calendar.html">Calendar</a></li>
                                <li><a href="apps-tickets.html">Tickets</a></li>
                                <li><a href="apps-taskboard.html">Task Board</a></li>
                                <li><a href="apps-task-detail.html">Task Detail</a></li>
                                <li><a href="apps-contacts.html">Contacts</a></li>
                                <li><a href="apps-projects.html">Projects</a></li>
                                <li><a href="apps-companies.html">Companies</a></li>
                                <li><a href="apps-file-manager.html">File Manager</a></li>
                            </ul>
                        </li> --}}

                    </ul>
                    <!-- End navigation menu -->
                </div>
                <!-- end #navigation -->
            </div>
            <!-- end container -->
        </div>
        <!-- end navbar-custom -->
    </header>
    <!-- End Navigation Bar-->

    <div class="wrapper">
        <div class="container-fluid">

            <!-- Page-Title -->
            @yield('breadcrumb')
            <!-- end page title end breadcrumb -->

            @yield('content')

        </div>
        <!-- end container -->
    </div>
    <!-- end wrapper -->


    <!-- Footer -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    2018 © UNPRG - Control de Seguimiento de Tesis
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->


    <!-- jQuery  -->
    <script src="{{ asset('highdmin/js/jquery.min.js') }}"></script>
    <script src="{{ asset('highdmin/js/popper.min.js') }}"></script>
    <script src="{{ asset('highdmin/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('highdmin/js/waves.js') }}"></script>
    <script src="{{ asset('highdmin/js/jquery.slimscroll.js') }}"></script>

    @yield('plugins-scripts')

    <!-- App js -->
    <script src="{{ asset('highdmin/js/jquery.core.js') }}"></script>
    <script src="{{ asset('highdmin/js/jquery.app.js') }}"></script>

    @yield('scripts')

    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>
