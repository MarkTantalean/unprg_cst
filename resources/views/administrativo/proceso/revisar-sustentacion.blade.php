@extends('layouts.app')

@section('plugins-styles')
<link href="{{ asset('highdmin/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('highdmin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('highdmin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('highdmin/plugins/clockpicker/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('highdmin/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">CST</a></li>
                    <li class="breadcrumb-item">Tesis</li>
                    <li class="breadcrumb-item active">Proceso</li>
                </ol>
            </div>
            <h4 class="page-title">Proceso de Tesis</h4>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="card-body">
                <h4 class="header-title mb-4">Proceso de {{ $proceso->tipoproceso->descripcion }}</h4>
                <div class="caja-proceso">
                    <div class="caja-inner">
                        <div class="caja-inner__header">
                            <h5>Datos del Alumno</h5>
                        </div>
                        <div class="caja-inner__body">
                            <p><b>Alumno: </b> {{ $proceso->tesis->alumno->persona->nombreCompleto() }}</p>
                            <p><b>Código: </b> {{ $proceso->tesis->alumno->getAttributes()['codigo'] }}</p>
                        </div>
                    </div>
                    <div class="caja-inner">
                        <div class="caja-inner__header">
                            <h5>Datos de la Solicitud</h5>
                        </div>
                        <div class="caja-inner__body">
                            ---
                        </div>
                    </div>
                    <div class="caja-inner">
                        <div class="caja-inner__header">
                            <h5>Responder</h5>
                        </div>
                        <div class="caja-inner__body">
                            @if ($errors->any())
                                <div class="card m-b-30 text-white bg-danger text-xs-center">
                                    <div class="card-body">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            <form action="{{ route('admin.proceso.sustentacion') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="proceso_id" value="{{ $proceso->id }}">
                                <div class="form-group clearfix">
                                    <p class="mb-2 mt-4 font-weight-bold">Resolución *</p>
                                    <input type="file" class="filestyle {{ $errors->has('urlResolucion') ? 'is-invalid' : ''}}" data-disabled="false" data-btnClass="btn-light" data-text="Seleccionar" name="urlResolucion">
                                    {!! $errors->first('urlResolucion', '<div class="invalid-feedback">:message</div>') !!}
                                </div>

                                <div class="form-group">
                                    <label class="control-label" for="fecha">Fecha Sustentación *</label>
                                    <div>
                                        <div class="input-group">
                                            <input type="text" for="fecha" name="fecha" class="form-control" placeholder="dd/mm/yyyy" id="fecha">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="form-group">
                                    <label for="horaIni">Hora de Inicio *</label>
                                    <div class="input-group clockpicker">
                                        <input type="text" class="form-control" name="horaIni" id="horaIni">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="mdi mdi-clock"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="horaFin">Hora de Fin *</label>
                                    <div class="input-group clockpicker">
                                        <input type="text" class="form-control" name="horaFin" id="horaFin">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="mdi mdi-clock"></i></span>
                                        </div>
                                    </div>
                                </div> --}}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="horaIni">Hora Inicio *</label>
                                            <input type="text" name="horaIni" id="horaIni" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="horaFin">Hora Fin *</label>
                                            <input type="text" name="horaFin" id="horaFin" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="control-label" for="observaciones">Observaciones</label>
                                    <div class="">
                                        <input class="form-control {{ $errors->has('observaciones') ? 'is-invalid' : ''}}" id="observaciones" name="observaciones" type="text">
                                        {!! $errors->first('observaciones', '<div class="invalid-feedback">:message</div>') !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="estado" class="col-form-label">Estado *</label>
                                    <select id="estado" name="estado" class="form-control">
                                        <option value="AC" selected>Aceptado</option>
                                        <option value="RE">Rechazado</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-custom waves-light waves-effect">Enviar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugins-scripts')
<script src="{{ asset('highdmin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>
<script src="{{ asset('highdmin/plugins/moment/moment.js') }}"></script>
<script src="{{ asset('highdmin/plugins/bootstrap-timepicker/bootstrap-timepicker.js') }}"></script>
<script src="{{ asset('highdmin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('highdmin/plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
<script src="{{ asset('highdmin/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('highdmin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<script>
    jQuery(document).ready(function () {
        $.fn.datepicker.dates['es'] = {
            days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"],
            daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Domingo"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
            today: "Hoy"
        };

        $(":file").filestyle({input: false});

        $('#fecha').datepicker({
            format: 'dd/mm/yyyy',
            language: 'es',
            autoclose: true,
            todayHighlight: true
        });

        $('#horaIni, #horaFin').timepicker({
            icons: {
                up: 'mdi mdi-chevron-up',
                down: 'mdi mdi-chevron-down'
            }
        });
    });
</script>
@endsection
