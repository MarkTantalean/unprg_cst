@extends('layouts.app')

@section('breadcrumb')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">CST</a></li>
                    <li class="breadcrumb-item">Tesis</li>
                    <li class="breadcrumb-item active">Proceso</li>
                </ol>
            </div>
            <h4 class="page-title">Proceso de Tesis</h4>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="card-body">
                <h4 class="header-title mb-4">Proceso de {{ $proceso->tipoproceso->descripcion }}</h4>
                <div class="caja-proceso">
                    <div class="caja-inner">
                        <div class="caja-inner__header">
                            <h5>Datos del Alumno</h5>
                        </div>
                        <div class="caja-inner__body">
                            <p><b>Alumno: </b> {{ $proceso->tesis->alumno->persona->nombreCompleto() }}</p>
                            <p><b>Código: </b> {{ $proceso->tesis->alumno->getAttributes()['codigo'] }}</p>
                        </div>
                    </div>
                    <div class="caja-inner">
                        <div class="caja-inner__header">
                            <h5>Datos de la Solicitud</h5>
                        </div>
                        <div class="caja-inner__body">
                            <p><b>Foto de Boucher de Pago: </b> <a class="btn btn-info btn-rounded waves-light waves-effect" href="{{ '/storage/' . $proceso->solicitudJurado->urlBoucher}}" download>Descargar</a>
                                <br>
                                <img width="300" src="{{ '/storage/' . $proceso->solicitudJurado->urlBoucher}}" alt=""></p>
                        </div>
                    </div>
                    <div class="caja-inner">
                        <div class="caja-inner__header">
                            <h5>Responder</h5>
                        </div>
                        <div class="caja-inner__body">
                            @if ($errors->any())
                                <div class="card m-b-30 text-white bg-danger text-xs-center">
                                    <div class="card-body">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            <form action="{{ route('admin.proceso.jurado') }}" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="proceso_id" value="{{ $proceso->id }}">
                                <div class="form-group clearfix">
                                    <p class="mb-2 mt-4 font-weight-bold">Resolución *</p>
                                    <input type="file" class="filestyle {{ $errors->has('urlResolucion') ? 'is-invalid' : ''}}" data-disabled="false" data-btnClass="btn-light" data-text="Seleccionar" name="urlResolucion">
                                    {!! $errors->first('urlResolucion', '<div class="invalid-feedback">:message</div>') !!}
                                </div>
                                <div class="form-group">
                                    <label for="presidente" class="col-form-label">Presidente *</label>
                                    <select id="presidente" name="jurado[presidente]" class="form-control">
                                        @foreach ($docentes as $doc)
                                            <option value="{{ $doc->getAttributes()['codigo'] }}">{{ $doc->persona->nombreCompleto() }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="secretario" class="col-form-label">Secretario *</label>
                                    <select id="secretario" name="jurado[secretario]" class="form-control">
                                        @foreach ($docentes as $doc)
                                            <option value="{{ $doc->getAttributes()['codigo'] }}">{{ $doc->persona->nombreCompleto() }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="vocal" class="col-form-label">Vocal *</label>
                                    <select id="vocal" name="jurado[vocal]" class="form-control">
                                        @foreach ($docentes as $doc)
                                            <option value="{{ $doc->getAttributes()['codigo'] }}">{{ $doc->persona->nombreCompleto() }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group clearfix">
                                    <label class="control-label" for="observaciones">Observaciones</label>
                                    <div class="">
                                        <input class="form-control {{ $errors->has('observaciones') ? 'is-invalid' : ''}}" id="observaciones" name="observaciones" type="text">
                                        {!! $errors->first('observaciones', '<div class="invalid-feedback">:message</div>') !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="estado" class="col-form-label">Estado *</label>
                                    <select id="estado" name="estado" class="form-control">
                                        <option value="AC" selected>Aceptado</option>
                                        <option value="RE">Rechazado</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-custom waves-light waves-effect">Enviar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugins-scripts')
<script src="{{ asset('highdmin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>
<script>
    jQuery(document).ready(function () {
        $(":file").filestyle({input: false});
    });
</script>
@endsection
