@extends('layouts.app')
@section('plugins-styles')
<!-- DataTables -->
<link href="{{ asset('highdmin/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
<link href="{{ asset('highdmin/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" />
<!-- Responsive datatable examples -->
<link href="{{ asset('highdmin/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" />

<!-- Multi Item Selection examples -->
<link href="{{ asset('highdmin/plugins/datatables/select.bootstrap4.min.css"') }} rel="stylesheet" />
@endsection


@section('breadcrumb')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">CST</a></li>
                    <li class="breadcrumb-item active">Tesis</li>
                </ol>
            </div>
            <h4 class="page-title">Tesis</h4>
        </div>
    </div>
</div>
@endsection





@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box table-responsive">
            <div class="card-body">
                <h4 class="header-title mb-4">Tesis Aceptadas</h4>
                <table id="tesis-pendientes" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Código</th>
                            <th>Alumno</th>
                            <th>Fecha</th>
                            <th>Proceso</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $index = 1;
                        @endphp
                        @forelse ($procesos as $p)
                        <tr>
                            <td>{{ $index }}</td>
                            <td>{{ $p->tesis->alumno->getAttributes()['codigo'] }}</td>
                            <td>{{ $p->tesis->alumno->persona->nombreCompleto() }}</td>
                            <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p->fechaModificacion)->format('d/m/Y H:i:s') }}</td>
                            <td>{{ $p->tipoProceso->descripcion }}</td>
                            <td>{{
                                array_get([
                                    'PE' => 'Pendiente',
                                    'PR' => 'Proceso',
                                    'RE' => 'Rechazado',
                                    'AC' => 'Aceptado'
                                ], $p->estado)
                            }}</td>
                        </tr>
                        @php
                            $index += 1;
                        @endphp
                        @empty
                        <tr>
                            <td colspan="5">No hay procesos de tesis aceptados</td>
                        </tr>
                        @endforelse
                    </tbody>
            </div>
        </div>
    </div>
</div>
@endsection





@section('plugins-scripts')
<!-- Required datatable js -->
<script src="{{ asset('highdmin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('highdmin/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('highdmin/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('highdmin/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('highdmin/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('highdmin/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('highdmin/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('highdmin/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('highdmin/plugins/datatables/buttons.print.min.js') }}"></script>

<!-- Key Tables -->
<script src="{{ asset('highdmin/plugins/datatables/dataTables.keyTable.min.js') }}"></script>

<!-- Responsive examples -->
<script src="{{ asset('highdmin/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('highdmin/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Selection table -->
<script src="{{ asset('highdmin/plugins/datatables/dataTables.select.min.js') }}"></script>
@endsection

