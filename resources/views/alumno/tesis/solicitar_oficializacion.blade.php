@extends('layouts.app')

@section('breadcrumb')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">CST</a></li>
                    <li class="breadcrumb-item active">Tesis</li>
                </ol>
            </div>
            <h4 class="page-title">Tesis</h4>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="card-body">
                <h4 class="header-title mb-4">Paso 3 - Solicitar Oficialización</h4>
                @if ($solicitud != null)
                    @if ($solicitud->estado == 'PE' || $solicitud->estado == 'PR')
                        <p>Usted ya ha enviado su solicitud, por favor espere a que esta sea atendida.</p>
                    @else
                        <div class="card m-b-30 text-white bg-danger text-xs-center">
                            <div class="card-body">
                                <div>RECHAZADO</div>
                            </div>
                        </div>
                        @if ($errors->any())
                            <div class="card m-b-30 text-white bg-danger text-xs-center">
                                <div class="card-body">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                        <form action="{{ route('alumno.tesis.oficializacion') }}" method="POST">
                            @csrf
                            <div class="form-group clearfix">
                                <label class="control-label" for="tesis">Tesis *</label>
                                <div class="">
                                    <input class="form-control {{ $errors->has('tesis') ? 'is-invalid' : ''}}" id="tesis" name="tesis" type="text" value="{{ $solicitud->tesis }}">
                                    {!! $errors->first('tesis', '<div class="invalid-feedback">:message</div>') !!}
                                </div>
                            </div>
                            <div class="form-group clearfix">
                                <p class="mb-2 mt-4 font-weight-bold">Ante Proyecto *</p>
                                <input type="file" class="filestyle {{ $errors->has('anteproyecto') ? 'is-invalid' : ''}}" data-disabled="false" data-btnClass="btn-light" data-text="Seleccionar" name="anteproyecto">
                                {!! $errors->first('anteproyecto', '<div class="invalid-feedback">:message</div>') !!}
                            </div>
                            <button type="submit" class="btn btn-custom waves-light waves-effect">Enviar</button>
                        </form>
                    @endif
                @else
                    @if ($errors->any())
                        <div class="card m-b-30 text-white bg-danger text-xs-center">
                            <div class="card-body">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <form action="{{ route('alumno.tesis.oficializacion') }}" method="POST">
                        @csrf
                        <div class="form-group clearfix">
                            <label class="control-label" for="tesis">Tesis *</label>
                            <div class="">
                                <input class="form-control {{ $errors->has('tesis') ? 'is-invalid' : ''}}" id="tesis" name="tesis" type="text" value="{{ old('tesis') }}">
                                {!! $errors->first('tesis', '<div class="invalid-feedback">:message</div>') !!}
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <p class="mb-2 mt-4 font-weight-bold">Ante Proyecto *</p>
                            <input type="file" class="filestyle {{ $errors->has('anteproyecto') ? 'is-invalid' : ''}}" data-disabled="false" data-btnClass="btn-light" data-text="Seleccionar" name="anteproyecto">
                            {!! $errors->first('anteproyecto', '<div class="invalid-feedback">:message</div>') !!}
                        </div>
                        <button type="submit" class="btn btn-custom waves-light waves-effect">Enviar</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugins-scripts')
<script src="{{ asset('highdmin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>
<script>
    jQuery(document).ready(function () {
        $(":file").filestyle({input: false});
    });
</script>
@endsection
