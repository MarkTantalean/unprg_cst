@extends('layouts.app')

@section('breadcrumb')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">CST</a></li>
                    <li class="breadcrumb-item active">Tesis</li>
                </ol>
            </div>
            <h4 class="page-title">Tesis</h4>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="card-body">
                {{-- <h4 class="header-title mb-4">Gestión de Tesis</h4> --}}
                <div class="card m-b-30 text-white bg-custom text-xs-center">
                    <div class="card-body">
                        Usted ha concluido con el proceso de gestión para la tesis
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugins-scripts')
@endsection
