@extends('layouts.app')

@section('breadcrumb')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">CST</a></li>
                    <li class="breadcrumb-item active">Tesis</li>
                </ol>
            </div>
            <h4 class="page-title">Tesis</h4>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="card-body">
                <h4 class="header-title mb-4">Paso 4 - Solicitar Sustentación</h4>
                @if ($solicitud != null)
                    @if ($solicitud->estado == 'PE' || $solicitud->estado == 'PR')
                        <p>Usted ya ha enviado su solicitud, por favor espere a que esta sea atendida.</p>
                    @else
                        <div class="card m-b-30 text-white bg-danger text-xs-center">
                            <div class="card-body">
                                <div>RECHAZADO</div>
                                {{-- <div>{{ $solicitud->observaciones }}</div> --}}
                            </div>
                        </div>
                        <form action="{{ route('alumno.tesis.sustentacion') }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-custom waves-light waves-effect">Enviar</button>
                        </form>
                    @endif
                @else
                    <form action="{{ route('alumno.tesis.sustentacion') }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-custom waves-light waves-effect">Enviar</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugins-scripts')
@endsection
