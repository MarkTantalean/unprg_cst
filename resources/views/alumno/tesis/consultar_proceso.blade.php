@extends('layouts.app')

@section('breadcrumb')
<div class="row">
    <div class="col-sm-12">
        <div class="page-title-box">
            <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                    <li class="breadcrumb-item"><a href="#">CST</a></li>
                    <li class="breadcrumb-item active">Tesis</li>
                </ol>
            </div>
            <h4 class="page-title">Tesis</h4>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card-box">
            <div class="card-body">
                <h4 class="header-title mb-4">Consultar estado de Tesis</h4>

                @isset($tesis)
                    {{-- <p><b>Titulo: </b>{{ $tesis->titulo }}</p> --}}
                    <div class="card card-proceso">
                        <div class="card-header">
                            <h5><span class="card-header__paso">Paso 1:</span> Solicitud de Asesor</h5>
                        </div>
                        @if ($so_asesor)
                            <div class="card-body">
                                <p><b>Fecha: </b>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $so_asesor->fechaModificacion)->format('d/m/Y H:i:s') }}</p>
                                <p><b>Asesor: </b>{{ $so_asesor->solicitudAsesor->asesor }}</p>
                                <p><b>Estado: </b>{{
                                    array_get([
                                        'PE' => 'Pendiente',
                                        'PR' => 'Proceso',
                                        'RE' => 'Rechazado',
                                        'AC' => 'Aceptado'
                                    ], $so_asesor->estado)
                                }}</p>
                                <p><b>Observaciones: </b>
                                    @if ($so_asesor->observaciones != "")
                                        {{ $so_asesor->observaciones }}
                                    @else
                                        ---
                                    @endif
                                </p>
                                <p><b>Resolucion: </b>
                                    @if ($so_asesor->estado == 'AC')
                                    <a class="btn btn-info btn-rounded waves-light waves-effect" href="{{ '/storage/' . $so_asesor->urlResolucion}}" download>Descargar</a>
                                    @else
                                        ---
                                    @endif
                                </p>
                            </div>
                        @endif
                    </div>
                    <div class="card card-proceso">
                        <div class="card-header">
                            <h5><span class="card-header__paso">Paso 2:</span> Solicitud de Jurado</h5>
                        </div>
                        @if ($so_jurado)
                            <div class="card-body">
                                <p><b>Fecha: </b>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $so_jurado->fechaModificacion)->format('d/m/Y H:i:s') }}</p>
                                <p>
                                    <b>Presidente: </b>
                                    @if ($so_jurado->jurados['presidente'] != null)
                                        {{ $so_jurado->jurados['presidente']->docente->persona->nombreCompleto() }}
                                    @else
                                        ---
                                    @endif
                                </p>
                                <p>
                                    <b>Secretario: </b>
                                    @if ($so_jurado->jurados['secretario'] != null)
                                        {{ $so_jurado->jurados['secretario']->docente->persona->nombreCompleto() }}
                                    @else
                                        ---
                                    @endif
                                </p>
                                <p>
                                    <b>Vocal: </b>
                                    @if ($so_jurado->jurados['vocal'] != null)
                                        {{ $so_jurado->jurados['vocal']->docente->persona->nombreCompleto() }}
                                    @else
                                        ---
                                    @endif
                                </p>
                                <p><b>Estado: </b>{{
                                    array_get([
                                        'PE' => 'Pendiente',
                                        'PR' => 'Proceso',
                                        'RE' => 'Rechazado',
                                        'AC' => 'Aceptado'
                                    ], $so_jurado->estado)
                                }}</p>
                                <p><b>Observaciones: </b>
                                    @if ($so_jurado->observaciones != "")
                                        {{ $so_jurado->observaciones }}
                                    @else
                                        ---
                                    @endif
                                </p>
                                <p><b>Resolucion: </b>
                                    @if ($so_jurado->estado == 'AC')
                                        <a class="btn btn-info btn-rounded waves-light waves-effect" href="{{ '/storage/' . $so_jurado->urlResolucion}}" download>Descargar</a>
                                    @else
                                        ---
                                    @endif
                                </p>
                            </div>
                        @endif
                    </div>
                    <div class="card card-proceso">
                        <div class="card-header">
                            <h5><span class="card-header__paso">Paso 3:</span> Solicitud de Oficialización</h5>
                        </div>
                        @if ($so_oficializacion)
                            <div class="card-body">
                                <p><b>Fecha: </b>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $so_oficializacion->fechaModificacion)->format('d/m/Y H:i:s') }}</p>
                                <p><b>Estado: </b>{{
                                    array_get([
                                        'PE' => 'Pendiente',
                                        'PR' => 'Proceso',
                                        'RE' => 'Rechazado',
                                        'AC' => 'Aceptado'
                                    ], $so_oficializacion->estado)
                                }}</p>
                                <p><b>Observaciones: </b>
                                    @if ($so_oficializacion->observaciones != "")
                                        {{ $so_oficializacion->observaciones }}
                                    @else
                                        ---
                                    @endif
                                </p>
                                <p><b>Resolucion: </b>
                                    @if ($so_oficializacion->estado == 'AC')
                                        <a class="btn btn-info btn-rounded waves-light waves-effect" href="{{ '/storage/' . $so_oficializacion->urlResolucion}}" download>Descargar</a>
                                    @else
                                        ---
                                    @endif
                                </p>
                            </div>
                        @endif
                    </div>
                    <div class="card card-proceso">
                        <div class="card-header">
                            <h5><span class="card-header__paso">Paso 4:</span> Solicitud de Sustentación</h5>
                        </div>
                        @if ($so_sustentacion)
                            <div class="card-body">
                                <p><b>Fecha: </b>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $so_sustentacion->fechaModificacion)->format('d/m/Y H:i:s') }}</p>
                                <p><b>Horario: </b>
                                    @if ($so_sustentacion->tesis->horario != null)
                                        {{ Carbon\Carbon::createFromFormat('Y-m-d', $so_sustentacion->tesis->horario->fecha)->format('d/m/Y') }} | {{
                                            Carbon\Carbon::createFromFormat('Y-m-d H:i:s', Carbon\Carbon::now()->toDateString() . ' ' . $so_sustentacion->tesis->horario->horaIni)
                                                ->format('H:i:s') . ' - ' .
                                            Carbon\Carbon::createFromFormat('Y-m-d H:i:s', Carbon\Carbon::now()->toDateString() . ' ' . $so_sustentacion->tesis->horario->horaFin)
                                                ->format('H:i:s')
                                        }}
                                    @else
                                        ---
                                    @endif
                                </p>
                                <p><b>Estado: </b>{{
                                    array_get([
                                        'PE' => 'Pendiente',
                                        'PR' => 'Proceso',
                                        'RE' => 'Rechazado',
                                        'AC' => 'Aceptado'
                                    ], $so_sustentacion->estado)
                                }}</p>
                                <p><b>Observaciones: </b>
                                    @if ($so_sustentacion->observaciones != "")
                                        {{ $so_sustentacion->observaciones }}
                                    @else
                                        ---
                                    @endif
                                </p>
                                <p><b>Resolucion: </b>
                                    @if ($so_sustentacion->estado == 'AC')
                                        <a class="btn btn-info btn-rounded waves-light waves-effect" href="{{ '/storage/' . $so_sustentacion->urlResolucion}}" download>Descargar</a>
                                    @else
                                        ---
                                    @endif
                                </p>
                            </div>
                        @endif
                            </div>
                    </div>
                @else
                    <div class="card m-b-30 text-white bg-custom text-xs-center bg-with-transparent">
                        <div class="card-body">
                            Usted no ha empezado el trámite para la tesis. Haga <a href="{{ route('alumno.tesis.gestionar') }}">click aquí</a>, si desea empezar.
                        </div>
                    </div>
                @endisset
            </div>
        </div>
    </div>
</div>
@endsection

@section('plugins-scripts')
@endsection
