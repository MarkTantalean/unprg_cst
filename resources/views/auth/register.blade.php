<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <title>Control de Seguimiento de Tesis</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content=" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('highdmin/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('highdmin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('highdmin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('highdmin/plugins/clockpicker/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('highdmin/plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

    <!-- App css -->
    <link href="{{ asset('highdmin/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('highdmin/css/icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('highdmin/css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('highdmin/js/modernizr.min.js') }}"></script>

</head>

<body>

    <div class="accountbg" style="background: url('img/logo.png'); background-color: #fff; background-repeat: no-repeat; background-position: 20% center; "></div>

    <div class="wrapper-page account-page-full">

        <div class="card">
            <div class="card-block">

                <div class="account-box">

                    <div class="card-box p-5">
                        <h2 class="text-center pb-4 title-login">
                            Registro de Alumno
                        </h2>

                        @if ($errors->any())
                            <div class="card m-b-30 text-white bg-danger text-xs-center">
                                <div class="card-body">
                                    @foreach ($errors->all() as $error)
                                        <span>{{ $error }}</span> <br>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                        <form class="form-horizontal" action="{{ route('registrar') }}" method="POST">

                            @csrf

                            <div class="form-group row m-b-20">
                                <div class="col-12">
                                    <label for="nombre">Nombre</label>
                                    <input class="form-control" type="text" id="nombre" name="nombre" value="{{ old('nombre') }}" required>
                                </div>
                            </div>

                            <div class="form-group row m-b-20">
                                <div class="col-md-6">
                                    <label for="apPaterno">Apellido Paterno</label>
                                    <input class="form-control" type="text" id="apPaterno" name="apPaterno" value="{{ old('apPaterno') }}" required>
                                </div>

                                <div class="col-md-6">
                                    <label for="apMaterno">Apellido Materno</label>
                                    <input class="form-control" type="text" id="apMaterno" name="apMaterno" value="{{ old('apMaterno') }}" required>
                                </div>
                            </div>

                            <div class="form-group row m-b-20">
                                <div class="col-md-6">
                                    <label for="sexo">Sexo</label>
                                    <select id="sexo" name="sexo" value="{{ old('sexo') }}" class="form-control">
                                        <option value="F">Femenino</option>
                                        <option value="M">Masculino</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label" for="fechaNacimiento">Fecha Nacimiento</label>
                                    <div>
                                        <div class="input-group">
                                            <input type="text" for="fechaNacimiento" name="fechaNacimiento" value="{{ old('fechaNacimiento') }}" class="form-control" placeholder="dd/mm/yyyy" id="fechaNacimiento">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row m-b-20">
                                <div class="col-md-6">
                                    <label for="codigo">Código</label>
                                    <input class="form-control" type="text" name="codigo" value="{{ old('codigo') }}" id="codigo" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="escuela">Escuela</label>
                                    <select id="escuela" name="escuela" value="{{ old('escuela') }}" class="form-control">
                                        @foreach ($escuelas as $item)
                                            <option value="{{ $item->getAttributes()['codigo'] }}">{{ $item->nombre }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row m-b-20">
                                <div class="col-12">
                                    <label for="password">Contraseña</label>
                                    <input class="form-control" type="password" required="" id="password" name="password" value="{{ old('password') }}">
                                </div>
                            </div>

                            <div class="form-group row text-center m-t-10">
                                <div class="col-12">
                                    <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Registrar</button>
                                </div>
                            </div>

                        </form>

                        <div class="row m-t-50">
                            <div class="col-sm-12 text-center">
                                <p class="text-muted">Ya tienes tu cuenta?  <a href="{{ route('principal') }}" class="text-dark m-l-5"><b>Inicia Sesión</b></a></p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>



    <!-- jQuery  -->
    <script src="{{ asset('highdmin/js/jquery.min.js') }}"></script>
    <script src="{{ asset('highdmin/js/popper.min.js') }}"></script>
    <script src="{{ asset('highdmin/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('highdmin/js/waves.js') }}"></script>
    <script src="{{ asset('highdmin/js/jquery.slimscroll.js') }}"></script>

    <script src="{{ asset('highdmin/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}"></script>
    <script src="{{ asset('highdmin/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('highdmin/plugins/bootstrap-timepicker/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('highdmin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('highdmin/plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
    <script src="{{ asset('highdmin/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('highdmin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        jQuery(document).ready(function () {
            $.fn.datepicker.dates['es'] = {
                days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"],
                daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Domingo"],
                daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
                months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"],
                monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Set", "Oct", "Nov", "Dic"],
                today: "Hoy"
            };

            $('#fechaNacimiento').datepicker({
                format: 'dd/mm/yyyy',
                language: 'es',
                autoclose: true,
                todayHighlight: true
            });

        });
    </script>

    <!-- App js -->
    <script src="{{ asset('highdmin/js/jquery.core.js') }}"></script>
    <script src="{{ asset('highdmin/js/jquery.app.js') }}"></script>

</body>

</html>
