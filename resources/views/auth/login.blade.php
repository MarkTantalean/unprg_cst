<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <title>Control de Seguimiento de Tesis</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content=" name="description" />
    <meta content="" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- App css -->
    <link href="{{ asset('highdmin/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('highdmin/css/icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('highdmin/css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('highdmin/js/modernizr.min.js') }}"></script>

</head>

<body>

    <div class="accountbg" style="background: url('img/logo.png'); background-color: #fff; background-repeat: no-repeat; background-position: 20% center; "></div>

    <div class="wrapper-page account-page-full">

        <div class="card">
            <div class="card-block">

                <div class="account-box">

                    <div class="card-box p-5">
                        <h2 class="text-center pb-4">
                            <p class="title-login">
                                Control de Seguimiento de Tesis
                            </p>
                        </h2>

                        <form class="frm-login" action="{{ route('login') }}" method="POST">
                            @csrf
                            <div class="form-group m-b-20 row">
                                <div class="col-12">
                                    <label for="usuario">Usuario</label>
                                    <input class="form-control" type="text" id="usuario" name="login" required placeholder="Ingresa tu código" value="{{ old('login') }}">
                                </div>
                            </div>

                            <div class="form-group row m-b-20">
                                <div class="col-12">
                                    <label for="password">Password</label>
                                    <input class="form-control" type="password" required id="password" name="password" placeholder="Ingresa tu contraseña">
                                </div>
                            </div>

                            @if ($errors->any())
                                <div class="card m-b-30 text-white bg-danger text-xs-center">
                                    <div class="card-body">
                                        @foreach ($errors->all() as $error)
                                            <span>{{ $error }}</span> <br>
                                        @endforeach
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row text-center m-t-10">
                                <div class="col-12">
                                    <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Ingresar</button>
                                </div>
                            </div>

                        </form>

                        <div class="row m-t-50">
                            <div class="col-sm-12 text-center">
                                <p class="text-muted">No tienes tu cuenta? <a href="{{ route('registrar') }}" class="text-dark m-l-5"><b>Registrate aquí</b></a></p>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="m-t-40 text-center">
            <p class="account-copyright">&nbsp</p>
        </div>

    </div>

    <!-- jQuery  -->
    <script src="{{ asset('highdmin/js/jquery.min.js') }}"></script>
    <script src="{{ asset('highdmin/js/popper.min.js') }}"></script>
    <script src="{{ asset('highdmin/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('highdmin/js/waves.js') }}"></script>
    <script src="{{ asset('highdmin/js/jquery.slimscroll.js') }}"></script>

    <!-- App js -->
    <script src="{{ asset('highdmin/js/jquery.core.js') }}"></script>
    <script src="{{ asset('highdmin/js/jquery.app.js') }}"></script>

</body>

</html>
