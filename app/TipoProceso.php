<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoProceso extends Model
{
    protected $table = 'tipoproceso';
    protected $primaryKey = 'id';
    public $timestamps = false;

    const SOLICITUD_ASESOR = 1;
    const SOLICITUD_JURADO = 2;
    const SOLICITUD_OFICIALIZACION = 3;
    const SOLICITUD_SUSTENTACION = 4;

    protected $fillable = [
        'descripcion'
    ];
}
