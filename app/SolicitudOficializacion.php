<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudOficializacion extends Model
{
    protected $table = 'solicitudoficializacion';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'tesis', 'anteproyecto', 'proceso_id'
    ];

}
