<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escuela extends Model
{
    protected $table = 'escuela';
    protected $primaryKey = 'codigo';
    public $timestamps = false;

    protected $fillable = [
        'codigo', 'nombre', 'abreviatura'
    ];

}
