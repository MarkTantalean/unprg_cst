<?php

namespace App;

use App\Persona;
use Illuminate\Database\Eloquent\Model;

class Administrativo extends Model
{
    protected $table = 'administrativo';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'persona_id', 'usuario_id'
    ];

    public function persona()
    {
        #pertenece a
        return $this->belongsTo(Persona::class, 'persona_id', 'id');
    }
}
