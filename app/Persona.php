<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'persona';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'nombres', 'apPaterno', 'apMaterno', 'sexo', 'fechaNacimiento'
    ];

    public function nombreCompleto()
    {
        return $this->nombres . ' ' . $this->apPaterno . ' ' . $this->apMaterno;
    }

}
