<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FuncionJurado extends Model
{
    protected $table = 'funcionjurado';
    protected $primaryKey = 'id';
    public $timestamps = false;

    const PRESIDENTE = 1;
    const SECRETARIO = 2;
    const VOCAL = 3;

    protected $fillable = [
        'descripcion'
    ];
}
