<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudAsesor extends Model
{
    protected $table = 'solicitudasesor';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'asesor', 'urlBachiller', 'proceso_id'
    ];

}
