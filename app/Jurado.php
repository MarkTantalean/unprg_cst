<?php

namespace App;

use App\Docente;
use Illuminate\Database\Eloquent\Model;

class Jurado extends Model
{
    protected $table = 'jurado';
    protected $primaryKey = ['docente_codigo', 'tesis_id'];
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'docente_codigo', 'tesis_id', 'funcion_id'
    ];

    public function docente()
    {
        return $this->belongsTo(Docente::class, 'docente_codigo', 'codigo');
    }
}
