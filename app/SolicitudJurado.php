<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudJurado extends Model
{
    protected $table = 'solicitudjurado';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'urlBoucher', 'proceso_id'
    ];

}
