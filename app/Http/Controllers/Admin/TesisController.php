<?php

namespace App\Http\Controllers\Admin;

use App\Tesis;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TesisController extends Controller
{
    public function gestionarTesisPendientes()
    {
        $procesos = $this->tesisConProcesoPendiente();
        return view('administrativo.tesis.pendientes', compact('procesos'));
    }

    public function consultarTesisAceptadas()
    {
        $procesos = $this->tesisConProcesosAceptados();
        return view('administrativo.tesis.aceptados', compact('procesos'));
    }

    public function consultarTesisRechazadas()
    {
        $procesos = $this->tesisConProcesosRechazados();
        return view('administrativo.tesis.rechazados', compact('procesos'));
    }

    public function tesisConProcesoPendiente()
    {
        $procesos = [];
        $tesis = Tesis::all();
        foreach ($tesis as $t) {
            foreach ($t->procesos as $p) {
                if ($p->estado != 'AC' && $p->estado != 'RE') {
                    $procesos[] = $p;
                }
            }
        }
        return collect($procesos)->sortBy('fechaModificacion');
    }

    public function tesisConProcesosAceptados()
    {
        $procesos = [];
        $tesis = Tesis::all();
        foreach ($tesis as $t) {
            foreach ($t->procesos as $p) {
                if ($p->estado == 'AC') {
                    $procesos[] = $p;
                }
            }
        }
        return collect($procesos)->sortBy('fechaModificacion');
    }

    public function tesisConProcesosRechazados()
    {
        $procesos = [];
        $tesis = Tesis::all();
        foreach ($tesis as $t) {
            foreach ($t->procesos as $p) {
                if ($p->estado == 'RE') {
                    $procesos[] = $p;
                }
            }
        }
        return collect($procesos)->sortBy('fechaModificacion');
    }
}
