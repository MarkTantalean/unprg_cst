<?php

namespace App\Http\Controllers\Admin;

use App\Jurado;
use App\Docente;
use App\Horario;
use App\Proceso;
use Carbon\Carbon;
use App\TipoProceso;
use App\FuncionJurado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ResponderSolicitudAsesorFormRequest;
use App\Http\Requests\ResponderSolicitudJuradoFormRequest;
use App\Http\Requests\ResponderSolicitudSustentacionFormRequest;
use App\Http\Requests\ResponderSolicitudOficializacionFormRequest;

class ProcesoController extends Controller
{
    public function revisar($idProceso)
    {
        $proceso = Proceso::findOrFail($idProceso);
        if ($proceso->estado != 'AC' && $proceso->estado != 'RE') {
            $proceso->estado = 'PR';
            $proceso->save();

            switch($proceso->tipoproceso_id) {
                case TipoProceso::SOLICITUD_ASESOR:
                    return view('administrativo.proceso.revisar-asesor', compact('proceso'));
                case TipoProceso::SOLICITUD_JURADO:
                    $docentes = Docente::all();
                    return view('administrativo.proceso.revisar-jurado', compact('proceso', 'docentes'));
                case TipoProceso::SOLICITUD_OFICIALIZACION:
                    return view('administrativo.proceso.revisar-oficializacion', compact('proceso'));
                case TipoProceso::SOLICITUD_SUSTENTACION:
                    return view('administrativo.proceso.revisar-sustentacion', compact('proceso'));
                default:
                    \abort(404);
            }
        }
        return \redirect()->route('admin.tesis.pendientes');
    }

    public function responderSolicitudAsesor(ResponderSolicitudAsesorFormRequest $request)
    {
        $data = $request->validated();
        if ($data['estado'] == 'AC' || $data['estado'] == 'RE') {
            $proceso = Proceso::findOrFail($data['proceso_id']);
            $proceso->observaciones = $data['observaciones'];
            $proceso->estado = $data['estado'];

            if ($data['estado'] == 'AC') {
                $codigo = $proceso->tesis->alumno->getAttributes()['codigo'];
                $proceso->urlResolucion = $this->guardarImagen($data['urlResolucion'], $codigo);
                $proceso->save();
                return \redirect()->route('admin.tesis.aceptadas');
            } else {
                $proceso->save();
                return \redirect()->route('admin.tesis.rechazadas');
            }
        }
        abort(404);
    }

    public function responderSolicitudJurado(ResponderSolicitudJuradoFormRequest $request)
    {
        $data = $request->validated();
        if ($data['estado'] == 'AC' || $data['estado'] == 'RE') {
            $proceso = Proceso::findOrFail($data['proceso_id']);
            $proceso->observaciones = $data['observaciones'];
            $proceso->estado = $data['estado'];

            if ($data['estado'] == 'AC') {
                $codigo = $proceso->tesis->alumno->getAttributes()['codigo'];
                $proceso->urlResolucion = $this->guardarImagen($data['urlResolucion'], $codigo);
                $this->guardarJurados($proceso, $data['jurado']);
                $proceso->save();
                return \redirect()->route('admin.tesis.aceptadas');
            } else {
                $proceso->save();
                return \redirect()->route('admin.tesis.rechazadas');
            }
        }
        abort(404);
    }

    public function guardarJurados($proceso, $jurados)
    {
        // obtener la tesis
        $tesis = $proceso->tesis;
        // eliminar jurados si existen
        Jurado::where('tesis_id', $tesis->id)->delete();
        // guardar los jurados
        Jurado::create([
            'docente_codigo' => $jurados['presidente'],
            'tesis_id' => $tesis->id,
            'funcion_id' => FuncionJurado::PRESIDENTE
        ]);
        Jurado::create([
            'docente_codigo' => $jurados['secretario'],
            'tesis_id' => $tesis->id,
            'funcion_id' => FuncionJurado::SECRETARIO
        ]);
        Jurado::create([
            'docente_codigo' => $jurados['vocal'],
            'tesis_id' => $tesis->id,
            'funcion_id' => FuncionJurado::VOCAL
        ]);
    }

    public function responderSolicitudOficializacion(ResponderSolicitudOficializacionFormRequest $request)
    {
        $data = $request->validated();
        if ($data['estado'] == 'AC' || $data['estado'] == 'RE') {
            $proceso = Proceso::findOrFail($data['proceso_id']);
            $proceso->observaciones = $data['observaciones'];
            $proceso->estado = $data['estado'];

            if ($data['estado'] == 'AC') {
                $codigo = $proceso->tesis->alumno->getAttributes()['codigo'];
                $proceso->urlResolucion = $this->guardarImagen($data['urlResolucion'], $codigo);
                $proceso->save();
                return \redirect()->route('admin.tesis.aceptadas');
            } else {
                $proceso->save();
                return \redirect()->route('admin.tesis.rechazadas');
            }
        }
        abort(404);
    }

    public function responderSolicitudSustentacion(Request $request)
    {
        $data = $this->getDatosValidados($request);

        if ($data['estado'] == 'AC' || $data['estado'] == 'RE') {
            $proceso = Proceso::findOrFail($data['proceso_id']);
            $proceso->observaciones = $data['observaciones'];
            $proceso->estado = $data['estado'];

            if ($data['estado'] == 'AC') {
                $codigo = $proceso->tesis->alumno->getAttributes()['codigo'];
                $proceso->urlResolucion = $this->guardarImagen($data['urlResolucion'], $codigo);
                if ($this->noHayCruces($proceso, $data)) {
                    $this->guardarHorarioSustentacion($proceso, $data);
                    $proceso->save();
                    return \redirect()->route('admin.tesis.aceptadas');
                } else {
                    return redirect()
                        ->back()
                        ->withErrors(['horaIni' => 'El horario seleccionado se cruza a los jurados con la sustentacion de otra tesis']);
                }
            } else {
                $proceso->save();
                return \redirect()->route('admin.tesis.rechazadas');
            }
        }
        abort(404);
    }

    private function getDatosValidados($request)
    {
        $request['horaIni'] = Carbon::parse($request['horaIni'])->format('H:i');
        $request['horaFin'] = Carbon::parse($request['horaFin'])->format('H:i');

        return $request->validate([
            'proceso_id' => 'required',
            'observaciones' => 'nullable',
            'fecha' => 'required|date_format:d/m/Y',
            'horaIni' => 'required|date_format:H:i',
            'horaFin' => 'required|date_format:H:i|after:horaIni',
            'estado' => 'required|string|min:2|max:2',
            'urlResolucion' => 'image|required_if:estado,AC',
        ], [
            'urlResolucion.required_if' => 'Debe subir una resolucion',
            'horaFin.after' => 'La hora fin debe ser una hora posterior a la hora de inicio'
        ]);
    }

    private function noHayCruces($proceso, $data)
    {
        $noCruces = true;
        $jurados = $proceso->tesis->jurados;
        foreach ($jurados as $jurado) {
            $fecha = Carbon::createFromFormat('d/m/Y', $data['fecha']);
            $tesis = Jurado::join('tesis', 'jurado.tesis_id', '=', 'tesis.id')
                                ->join('horario', 'tesis.horario_id', '=', 'horario.id')
                                ->where('jurado.docente_codigo', $jurado->getAttributes()['docente_codigo'])
                                ->where('horario.fecha', $fecha->toDateString())
                                ->where('horario.horaIni', '<', $data['horaFin'])
                                ->where('horario.horaFin', '>', $data['horaIni'])
                                ->get();
            if(count($tesis)) {
                $noCruces = false;
                break;
            }
        }
        return $noCruces;
    }

    private function guardarHorarioSustentacion($proceso, $data)
    {
        $tesis = $proceso->tesis;
        $fecha = Carbon::createFromFormat('d/m/Y', $data['fecha']);
        $horario = new Horario;
        $horario->fecha = $fecha->toDateString();
        $horario->horaIni = $data['horaIni'];
        $horario->horaFin = $data['horaFin'];
        $horario->save();

        $tesis->horario_id = $horario->id;
        $tesis->save();
    }
}
