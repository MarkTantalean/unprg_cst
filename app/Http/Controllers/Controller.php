<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function guardarImagen($file, $codigo)
    {
        # obtenemos el nombre del archivo
        $nombre = $file->getClientOriginalName();
        # creamos path url del archivo
        $urlShort = $codigo . '/' . $nombre;
        # indicamos que queremos guardar un nuevo archivo en el disco local
        $ruta = Storage::disk('local')->put('public/' . $urlShort, \File::get($file));
        # retornamos la url
        return $urlShort;
    }
}
