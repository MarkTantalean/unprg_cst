<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Alumno;
use App\Escuela;
use App\Persona;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\RegistroAlumnoFormRequest;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $escuelas = Escuela::all();
        return view('auth.register', \compact('escuelas'));
    }

    protected function create(array $data)
    {
        $user = User::create([
            'login' => $data['codigo'],
            'password' => Hash::make($data['password']),
            'tipo_usuario' => 'A'
        ]);

        $persona = Persona::create([
            'nombres' => $data['nombre'],
            'apPaterno' => $data['apPaterno'],
            'apMaterno' => $data['apMaterno'],
            'sexo' => $data['sexo'],
            'fechaNacimiento' => Carbon::createFromFormat('d/m/Y', $data['fechaNacimiento'])->toDateString('Y-m-d'),
        ]);

        Alumno::create([
            'codigo' => $data['codigo'],
            'persona_id' => $persona->id,
            'usuario_id' => $user->id,
            'escuela_codigo' => $data['escuela'],
        ]);

        return $user;
    }

    public function register(RegistroAlumnoFormRequest $request)
    {
        $data = $request->validated();

        event(new Registered($user = $this->create($data)));

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }
}
