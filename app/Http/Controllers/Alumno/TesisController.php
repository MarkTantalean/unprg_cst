<?php

namespace App\Http\Controllers\Alumno;

use App\Tesis;
use App\Jurado;
use App\Proceso;
use Carbon\Carbon;
use App\TipoProceso;
use App\FuncionJurado;
use App\SolicitudAsesor;
use App\SolicitudJurado;
use Illuminate\Http\Request;
use App\SolicitudSustentacion;
use App\SolicitudOficializacion;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\SolicitudAsesorFormRequest;
use App\Http\Requests\SolicitudJuradoFormRequest;
use App\Http\Requests\SolicitudOficializacionFormRequest;

class TesisController extends Controller
{

    public function gestionar()
    {
        $codigo = Auth::user()->usuario->getAttributes()['codigo'];
        $tesis = Tesis::where('alumno_codigo', $codigo)->first();
        if ($tesis == null) {
            return view('alumno.tesis.solicitar_asesor', ['solicitud' => null]);
        } else {
            return $this->mostrarVistaProcesoActual($tesis);
        }
    }

    private function mostrarVistaProcesoActual($tesis)
    {
        $solicitudes = ['asesor', 'jurado', 'oficializacion', 'sustentacion'];
        foreach ($solicitudes as $solicitud) {
            $obj = $this->obtenerSolicitud($solicitud, $tesis->id);
            if ($obj == null || $obj->estado != 'AC') {
                return view('alumno.tesis.solicitar_' . $solicitud, ['solicitud' => $obj]);
            }
        }
        return view('alumno.tesis.proceso_terminado');
    }

    private function obtenerSolicitud($solicitud, $tesis_id)
    {
        switch($solicitud)
        {
            case 'asesor':
                return Proceso::where('tesis_id', $tesis_id)
                    ->where('tipoproceso_id', TipoProceso::SOLICITUD_ASESOR)
                    ->first();
            case 'jurado':
                return Proceso::where('tesis_id', $tesis_id)
                    ->where('tipoproceso_id', TipoProceso::SOLICITUD_JURADO)
                    ->first();
            case 'oficializacion':
                return Proceso::where('tesis_id', $tesis_id)
                    ->where('tipoproceso_id', TipoProceso::SOLICITUD_OFICIALIZACION)
                    ->first();
            case 'sustentacion':
                return Proceso::where('tesis_id', $tesis_id)
                    ->where('tipoproceso_id', TipoProceso::SOLICITUD_SUSTENTACION)
                    ->first();
            default:
                return null;
        }
    }

    # Guardar las solicitudes
    public function solicitarAsesor(SolicitudAsesorFormRequest $request)
    {
        $data = $request->validated(); // array
        $tesis = $this->guardarTesis($data);
        $solicitud = $this->guardarSolicitudAsesor($data, $tesis->id);
        return redirect()->back();
    }

    private function guardarTesis($data)
    {
        $codigo = Auth::user()->usuario->getAttributes()['codigo'];
        $tesis = Tesis::where('alumno_codigo', $codigo)->first();
        if ($tesis == null) {
            $tesis = new Tesis;
            $tesis->alumno_codigo = $codigo;
            $tesis->save();
        }
        return $tesis;
    }

    private function guardarSolicitudAsesor($data, $tesis_id)
    {
        $codigo = Auth::user()->usuario->getAttributes()['codigo'];
        $proceso = Proceso::where('tesis_id', $tesis_id)
                                ->where('tipoproceso_id', TipoProceso::SOLICITUD_ASESOR)
                                ->first();
        $solicitud = null;

        if ($proceso == null) {
            $proceso = new Proceso;
            $proceso->fechaCreacion = Carbon::now()->toDateTimeString();
            $proceso->fechaModificacion = Carbon::now()->toDateTimeString();
            $proceso->urlResolucion = '';
            $proceso->observaciones = '';
            $proceso->estado = 'PE';
            $proceso->tesis_id = $tesis_id;
            $proceso->tipoproceso_id = TipoProceso::SOLICITUD_ASESOR;
            $proceso->save();

            $solicitud = new SolicitudAsesor;
            $solicitud->proceso_id = $proceso->id;
        } else {
            $solicitud = SolicitudAsesor::where('proceso_id', $proceso->id)->first();
        }

        $proceso->fechaModificacion = Carbon::now()->toDateTimeString();
        $proceso->estado = 'PE';
        $proceso->save();

        $solicitud->asesor = $data['asesor'];
        $solicitud->urlBachiller = $this->guardarImagen($data['imgBachiller'], $codigo);
        $solicitud->save();
        return $solicitud;
    }

    public function solicitarJurado(SolicitudJuradoFormRequest $request)
    {
        #validamos los datos del formulario
        $data = $request->validated();
        #obtenemos el codigo del alumno logeado
        $codigo = Auth::user()->usuario->getAttributes()['codigo'];
        #obteneemos la tesis del alumno
        $tesis = Tesis::where('alumno_codigo', $codigo)->first();
        #obtenemos el proceso de solicitud de asesor para saber si ya fue aceptado
        $procesoAsesor = Proceso::where('tesis_id', $tesis->id)
                    ->where('tipoproceso_id', TipoProceso::SOLICITUD_ASESOR)
                    ->first();
        #validamos que el proceso de solicitud de asesor es aceptado para guardar la solicitud de Jurado
        if ($procesoAsesor->estado == 'AC') {
            $solicitud = $this->guardarSolicitudJurado($data, $tesis->id);
        }
        return redirect()->back();
    }

    private function guardarSolicitudJurado($data, $tesis_id)
    {
        #codigo de alumno
        $codigo = Auth::user()->usuario->getAttributes()['codigo'];
        #comprobamos si existe ya un proceso de solicitud de jurado
        $proceso = Proceso::where('tesis_id', $tesis_id)
                        ->where('tipoproceso_id', TipoProceso::SOLICITUD_JURADO)
                        ->first();
        $solicitud = null;
        if ($proceso == null) {
            $proceso = new Proceso;
            $proceso->fechaCreacion = Carbon::now()->toDateTimeString();
            $proceso->fechaModificacion = Carbon::now()->toDateTimeString();
            $proceso->urlResolucion = '';
            $proceso->observaciones = '';
            $proceso->estado = 'PE';
            $proceso->tesis_id = $tesis_id;
            $proceso->tipoproceso_id = TipoProceso::SOLICITUD_JURADO;
            $proceso->save();

            $solicitud = new SolicitudJurado;
            $solicitud->proceso_id = $proceso->id;
        } else {
            $solicitud = SolicitudJurado::where('proceso_id', $proceso->id)->first();
        }
        $proceso->fechaModificacion = Carbon::now()->toDateTimeString();
        $proceso->estado = 'PE';
        $proceso->save();

        $solicitud->urlBoucher = $this->guardarImagen($data['urlBoucher'], $codigo);
        $solicitud->save();
        return $solicitud;
    }

    public function solicitarOficializacion(SolicitudOficializacionFormRequest $request)
    {
        $data = $request->validated();
        $codigo = Auth::user()->usuario->getAttributes()['codigo'];
        $tesis = Tesis::where('alumno_codigo', $codigo)->first();
        $proceso = Proceso::where('tesis_id', $tesis->id)
                        ->where('tipoproceso_id', TipoProceso::SOLICITUD_JURADO)
                        ->first();
        if ($proceso->estado == 'AC') {
            $solicitud = $this->guardarSolicitudOficializacion($data, $tesis->id);
        }
        return redirect()->back();
    }

    private function guardarSolicitudOficializacion($data, $tesis_id)
    {
        $proceso = Proceso::where('tesis_id', $tesis_id)
                        ->where('tipoproceso_id', TipoProceso::SOLICITUD_OFICIALIZACION)
                        ->first();
        $solicitud = null;
        if ($proceso == null) {
            $proceso = new Proceso;
            $proceso->fechaCreacion = Carbon::now()->toDateTimeString();
            $proceso->fechaModificacion = Carbon::now()->toDateTimeString();
            $proceso->urlResolucion = '';
            $proceso->observaciones = '';
            $proceso->estado = 'PE';
            $proceso->tesis_id = $tesis_id;
            $proceso->tipoproceso_id = TipoProceso::SOLICITUD_OFICIALIZACION;
            $proceso->save();

            $solicitud = new SolicitudOficializacion;
            $solicitud->proceso_id = $proceso->id;
        } else {
            $solicitud = SolicitudOficializacion::where('proceso_id', $proceso->id)->first();
        }

        $proceso->fechaModificacion = Carbon::now()->toDateTimeString();
        $proceso->estado = 'PE';
        $proceso->save();

        $solicitud->tesis = $data['tesis'];
        $solicitud->anteproyecto = $data['anteproyecto'];
        $solicitud->save();
        return $proceso;
    }

    public function solicitarSustentacion(Request $request)
    {
        $codigo = Auth::user()->usuario->getAttributes()['codigo'];
        $tesis = Tesis::where('alumno_codigo', $codigo)->first();
        $proceso = Proceso::where('tesis_id', $tesis->id)
                        ->where('tipoproceso_id', TipoProceso::SOLICITUD_OFICIALIZACION)
                        ->first();
        if ($proceso->estado == 'AC') {
            $solicitud = $this->guardarSolicitudSustentacion($tesis->id);
        }
        return redirect()->back();
    }

    private function guardarSolicitudSustentacion($tesis_id)
    {
        $proceso = Proceso::where('tesis_id', $tesis_id)
                        ->where('tipoproceso_id', TipoProceso::SOLICITUD_SUSTENTACION)
                        ->first();
        if ($proceso == null) {
            $proceso = new Proceso;
            $proceso->fechaCreacion = Carbon::now()->toDateTimeString();
            $proceso->fechaModificacion = Carbon::now()->toDateTimeString();
            $proceso->urlResolucion = '';
            $proceso->observaciones = '';
            $proceso->estado = 'PE';
            $proceso->tesis_id = $tesis_id;
            $proceso->tipoproceso_id = TipoProceso::SOLICITUD_SUSTENTACION;
            $proceso->save();
        }

        $proceso->fechaModificacion = Carbon::now()->toDateTimeString();
        $proceso->estado = 'PE';
        $proceso->save();
        return $proceso;
    }

    public function consultar()
    {
        $codigo = Auth::user()->usuario->getAttributes()['codigo'];
        $tesis = Tesis::where('alumno_codigo', $codigo)->first();
        $datos = [];
        if ($tesis != null) {
            $datos['tesis'] = $tesis;
            $datos['so_asesor'] = Proceso::where('tesis_id', $tesis->id)
                ->where('tipoproceso_id', TipoProceso::SOLICITUD_ASESOR)
                ->first();
            $proceso = Proceso::where('tesis_id', $tesis->id)
                ->where('tipoproceso_id', TipoProceso::SOLICITUD_JURADO)
                ->first();
            if ($proceso != null) {
                $proceso->jurados = [
                    'presidente' => Jurado::where('tesis_id', $tesis->id)->where('funcion_id', FuncionJurado::PRESIDENTE)->first(),
                    'secretario' => Jurado::where('tesis_id', $tesis->id)->where('funcion_id', FuncionJurado::SECRETARIO)->first(),
                    'vocal' => Jurado::where('tesis_id', $tesis->id)->where('funcion_id', FuncionJurado::VOCAL)->first()
                ];
            }
            $datos['so_jurado'] = $proceso;
            $datos['so_oficializacion'] = Proceso::where('tesis_id', $tesis->id)
                ->where('tipoproceso_id', TipoProceso::SOLICITUD_OFICIALIZACION)
                ->first();
            $datos['so_sustentacion'] = Proceso::where('tesis_id', $tesis->id)
                ->where('tipoproceso_id', TipoProceso::SOLICITUD_SUSTENTACION)
                ->first();
        }
        return view('alumno.tesis.consultar_proceso', $datos);
    }
}
