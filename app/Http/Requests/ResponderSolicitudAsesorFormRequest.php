<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResponderSolicitudAsesorFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'proceso_id' => 'required',
            'urlResolucion' => 'image|required_if:estado,AC',
            'observaciones' => 'nullable',
            'estado' => 'required|string|min:2|max:2'
        ];
    }

    public function messages()
    {
        return [
            'urlResolucion.required_if' => 'Debe subir una resolucion',
        ];
    }
}
