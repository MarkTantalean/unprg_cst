<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResponderSolicitudSustentacionFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'proceso_id' => 'required',
            'observaciones' => 'nullable',
            'fecha' => 'required|date_format:d/m/Y',
            'horaIni' => 'required|date_format:H:i',
            'horaFin' => 'required|date_format:H:i|after:horaInicio',
            'estado' => 'required|string|min:2|max:2',
            'urlResolucion' => 'image|required_if:estado,AC',
        ];
    }

    public function messages()
    {
        return [
            'urlResolucion.required_if' => 'Debe subir una resolucion',
            'horaFin.after' => 'La hora fin debe ser una hora posterior a la hora de inicio'
        ];
    }
}
