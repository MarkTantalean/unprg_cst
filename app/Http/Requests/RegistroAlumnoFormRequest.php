<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistroAlumnoFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'apPaterno' => 'required',
            'apMaterno' => 'required',
            'sexo' => 'required',
            'fechaNacimiento' => 'required',
            'codigo' => 'required',
            'escuela' => 'required',
            'password' => 'required'
        ];
    }
}
