<?php

namespace App;

use App\Tesis;
use App\TipoProceso;
use Illuminate\Database\Eloquent\Model;

class Proceso extends Model
{
    protected $table = 'proceso';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'fechaCreacion', 'fechaModificacion', 'urlResolucion', 'observaciones', 'estado', 'tesis_id', 'tipoproceso_id'
    ];

    public function solicitudAsesor()
    {
        return $this->hasOne('App\SolicitudAsesor', 'proceso_id', 'id');
    }

    public function solicitudJurado()
    {
        return $this->hasOne('App\SolicitudJurado', 'proceso_id', 'id');
    }

    public function solicitudOficializacion()
    {
        return $this->hasOne('App\SolicitudOficializacion', 'proceso_id', 'id');
    }

    public function tesis()
    {
        return $this->belongsTo(Tesis::class, 'tesis_id', 'id');
    }

    public function tipoProceso()
    {
        return $this->belongsTo(TipoProceso::class, 'tipoproceso_id', 'id');
    }
}
