<?php

namespace App;

use App\Alumno;
use App\Horario;
use Illuminate\Database\Eloquent\Model;

class Tesis extends Model
{
    protected $table = 'tesis';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'alumno_codigo', 'horario_id'
    ];

    public function procesos()
    {
        return $this->hasMany('App\Proceso', 'tesis_id', 'id');
    }

    public function alumno()
    {
        return $this->belongsTo(Alumno::class, 'alumno_codigo', 'codigo');
    }

    public function jurados()
    {
        return $this->hasMany('App\Jurado');
    }

    public function horario()
    {
        return $this->hasOne(Horario::class, 'id', 'horario_id');
    }
}
