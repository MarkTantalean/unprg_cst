<?php

namespace App;

use App\Alumno;
use App\Administrativo;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'usuario';
    public $timestamps = false;
    public $remember_token = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'password', 'tipo_usuario',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function usuario()
    {
        if ($this->tipo_usuario == 'S') {
            return $this->hasOne(Administrativo::class, 'usuario_id', 'id');
        }

        if ($this->tipo_usuario == 'A') {
            return $this->hasOne(Alumno::class, 'usuario_id', 'id');
        }

        return null;
    }

    public function hasPermiso($ruta)
    {
        if ($this->tipo_usuario === 'S' && in_array($ruta, [
            'admin.tesis.pendientes',
            'admin.tesis.aceptadas',
            'admin.tesis.rechazadas',
            'admin.proceso.revisar',
            'admin.proceso.asesor',
            'admin.proceso.jurado',
            'admin.proceso.oficializacion',
            'admin.proceso.sustentacion',
        ])) {
            return true;
        }

        if ($this->tipo_usuario === 'A' && in_array($ruta, [
            'alumno.tesis.gestionar',
            'alumno.tesis.consultar',
            'alumno.tesis.asesor',
            'alumno.tesis.jurado',
            'alumno.tesis.oficializacion',
            'alumno.tesis.sustentacion'
        ])) {
            return true;
        }

        return false;
    }
}
