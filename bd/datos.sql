
insert into escuela values('06', 'Escuela de Computación e Informática', 'CI');

insert into funcionjurado values(null, 'Presidente');
insert into funcionjurado values(null, 'Secretario');
insert into funcionjurado values(null, 'Vocal');

insert into tipoproceso values(null, 'Solicitud de Asesor');
insert into tipoproceso values(null, 'Solicitud de Jurado');
insert into tipoproceso values(null, 'Solicitud de Oficialización');
insert into tipoproceso values(null, 'Solicitud de Sustentación');

insert into usuario values(null, 'admin', '$2y$10$T6SqYnBwijofo/PiOLWDxeQb34i8vXaK90QYOlNf7vduUdoXfEbSC', 'S');
insert into usuario values(null, '020140228E', '$2y$10$T6SqYnBwijofo/PiOLWDxeQb34i8vXaK90QYOlNf7vduUdoXfEbSC', 'A');
insert into usuario values(null, '020140228A', '$2y$10$T6SqYnBwijofo/PiOLWDxeQb34i8vXaK90QYOlNf7vduUdoXfEbSC', 'A');
insert into usuario values(null, '020140228C', '$2y$10$T6SqYnBwijofo/PiOLWDxeQb34i8vXaK90QYOlNf7vduUdoXfEbSC', 'A');

insert into persona values(null, 'Lucas', 'Suarez', 'Perez', 'M', '1974-05-07');
insert into persona values(null, 'Juan', 'Gonzales', 'Prada', 'M', '1994-02-03');
insert into persona values(null, 'Melvi', 'Medina', 'Paredes', 'F', '1995-08-03');
insert into persona values(null, 'Carlos', 'Saba', 'Melendez', 'M', '1992-03-04');

insert into administrativo values(null, 1, 1);
insert into alumno values('020140228E', 2, 2, '06');
insert into alumno values('020140228A', 3, 3, '06');
insert into alumno values('020140228C', 4, 4, '06');
	
select * from alumno;

-- DOCENTES
select * from persona;
insert into persona values(null, 'Carlos', 'Valdivia', 'Salazar', 'M', '1975-03-04');
insert into persona values(null, 'Franklin', 'Teran', 'Santa Cruz', 'M', '1975-03-04');
insert into persona values(null, 'Percy', 'Rodriguez', 'Melendez', 'M', '1975-03-04');
insert into persona values(null, 'Juan Carlos', 'Santisteban', 'Matallana', 'M', '1975-03-04');
insert into persona values(null, 'Betty', 'Suarez', 'Juarez', 'F', '1975-03-04');
insert into persona values(null, 'Ana Maria', 'Medina', 'Tesen', 'M', '1975-03-04');
insert into persona values(null, 'Erwin', 'Santa Cruz', 'Bermejo', 'M', '1975-03-04');
insert into persona values(null, 'Axel', 'Samame', 'Cojal', 'M', '1975-03-04');

insert into docente values('134565J', 5);
insert into docente values('123335J', 6);
insert into docente values('123445J', 7);
insert into docente values('332243J', 8);
insert into docente values('123154J', 9);
insert into docente values('167332J', 10);
insert into docente values('534121J', 11);
insert into docente values('135722J', 12);





