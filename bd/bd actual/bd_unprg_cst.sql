-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-12-2018 a las 13:00:43
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_unprg_cst`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrativo`
--

CREATE TABLE `administrativo` (
  `id` int(11) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `administrativo`
--

INSERT INTO `administrativo` (`id`, `persona_id`, `usuario_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `codigo` char(10) NOT NULL,
  `persona_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `escuela_codigo` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`codigo`, `persona_id`, `usuario_id`, `escuela_codigo`) VALUES
('020140225C', 15, 9, '06'),
('020140228A', 3, 3, '06'),
('020140228C', 4, 4, '06'),
('020140228E', 2, 2, '06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docente`
--

CREATE TABLE `docente` (
  `codigo` char(10) NOT NULL,
  `persona_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `docente`
--

INSERT INTO `docente` (`codigo`, `persona_id`) VALUES
('123154J', 9),
('123335J', 6),
('123445J', 7),
('134565J', 5),
('135722J', 12),
('167332J', 10),
('332243J', 8),
('534121J', 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escuela`
--

CREATE TABLE `escuela` (
  `codigo` char(2) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `abreviatura` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `escuela`
--

INSERT INTO `escuela` (`codigo`, `nombre`, `abreviatura`) VALUES
('06', 'Escuela de Computación e Informática', 'CI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcionjurado`
--

CREATE TABLE `funcionjurado` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `funcionjurado`
--

INSERT INTO `funcionjurado` (`id`, `descripcion`) VALUES
(1, 'Presidente'),
(2, 'Secretario'),
(3, 'Vocal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `horaIni` time NOT NULL,
  `horaFin` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`id`, `fecha`, `horaIni`, `horaFin`) VALUES
(1, '2018-12-19', '17:00:00', '19:00:00'),
(3, '2018-12-19', '20:45:00', '22:45:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jurado`
--

CREATE TABLE `jurado` (
  `docente_codigo` char(10) NOT NULL,
  `tesis_id` int(11) NOT NULL,
  `funcion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `jurado`
--

INSERT INTO `jurado` (`docente_codigo`, `tesis_id`, `funcion_id`) VALUES
('123154J', 1, 1),
('123154J', 2, 1),
('123154J', 3, 1),
('123335J', 1, 2),
('134565J', 2, 2),
('167332J', 3, 2),
('123445J', 3, 3),
('167332J', 2, 3),
('332243J', 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(11) NOT NULL,
  `nombres` varchar(45) NOT NULL,
  `apPaterno` varchar(45) NOT NULL,
  `apMaterno` varchar(45) NOT NULL,
  `sexo` char(1) NOT NULL,
  `fechaNacimiento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `nombres`, `apPaterno`, `apMaterno`, `sexo`, `fechaNacimiento`) VALUES
(1, 'Lucas', 'Suarez', 'Perez', 'M', '1974-05-07'),
(2, 'Juan', 'Gonzales', 'Prada', 'M', '1994-02-03'),
(3, 'Melvi', 'Medina', 'Paredes', 'F', '1995-08-03'),
(4, 'Carlos', 'Saba', 'Melendez', 'M', '1992-03-04'),
(5, 'Carlos', 'Valdivia', 'Salazar', 'M', '1975-03-04'),
(6, 'Franklin', 'Teran', 'Santa Cruz', 'M', '1975-03-04'),
(7, 'Percy', 'Rodriguez', 'Melendez', 'M', '1975-03-04'),
(8, 'Juan Carlos', 'Santisteban', 'Matallana', 'M', '1975-03-04'),
(9, 'Betty', 'Suarez', 'Juarez', 'F', '1975-03-04'),
(10, 'Ana Maria', 'Medina', 'Tesen', 'M', '1975-03-04'),
(11, 'Erwin', 'Santa Cruz', 'Bermejo', 'M', '1975-03-04'),
(12, 'Axel', 'Samame', 'Cojal', 'M', '1975-03-04'),
(15, 'asdasdsad', 'dasdasd', 'dasdasd', 'F', '2018-12-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proceso`
--

CREATE TABLE `proceso` (
  `id` int(11) NOT NULL,
  `fechaCreacion` datetime NOT NULL,
  `fechaModificacion` datetime NOT NULL,
  `urlResolucion` varchar(250) NOT NULL,
  `observaciones` varchar(250) DEFAULT NULL,
  `estado` char(2) NOT NULL,
  `tesis_id` int(11) NOT NULL,
  `tipoproceso_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proceso`
--

INSERT INTO `proceso` (`id`, `fechaCreacion`, `fechaModificacion`, `urlResolucion`, `observaciones`, `estado`, `tesis_id`, `tipoproceso_id`) VALUES
(1, '2018-12-10 16:12:49', '2018-12-10 16:12:49', '020140228E/escanear0002.jpg', NULL, 'AC', 1, 1),
(2, '2018-12-10 16:13:35', '2018-12-10 16:13:35', '020140228E/escanear0002.jpg', NULL, 'AC', 1, 2),
(3, '2018-12-10 16:14:30', '2018-12-10 16:14:30', '020140228E/wi_2.jpg', NULL, 'AC', 1, 3),
(4, '2018-12-10 16:14:55', '2018-12-10 16:14:55', '020140228E/escanear0004.jpg', NULL, 'AC', 1, 4),
(5, '2018-12-10 17:13:10', '2018-12-10 17:13:10', '020140228C/escanear0004.jpg', NULL, 'AC', 2, 1),
(6, '2018-12-10 17:14:03', '2018-12-10 17:14:03', '020140228C/wi_2.jpg', NULL, 'AC', 2, 2),
(7, '2018-12-10 17:15:15', '2018-12-10 17:15:15', '020140228C/wi_1.jpg', NULL, 'AC', 2, 3),
(8, '2018-12-10 17:15:59', '2018-12-10 17:15:59', '020140228C/escanear0004.jpg', NULL, 'AC', 2, 4),
(9, '2018-12-12 23:50:43', '2018-12-12 23:50:43', '020140228A/escanear0003.jpg', NULL, 'AC', 3, 1),
(10, '2018-12-12 23:52:35', '2018-12-12 23:52:35', '020140228A/escanear0003.jpg', NULL, 'AC', 3, 2),
(12, '2018-12-13 00:37:33', '2018-12-13 00:37:33', '020140228A/escanear0002.jpg', NULL, 'AC', 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudasesor`
--

CREATE TABLE `solicitudasesor` (
  `id` int(11) NOT NULL,
  `asesor` varchar(102) NOT NULL,
  `urlBachiller` varchar(250) NOT NULL,
  `proceso_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `solicitudasesor`
--

INSERT INTO `solicitudasesor` (`id`, `asesor`, `urlBachiller`, `proceso_id`) VALUES
(1, 'Carlos Valdivia Salazar', '020140228E/escanear0001.jpg', 1),
(2, 'Franklin Teran Mechan', '020140228C/escanear0001.jpg', 5),
(3, 'Carlos Valdivia Salazar', '020140228A/escanear0003.jpg', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudjurado`
--

CREATE TABLE `solicitudjurado` (
  `id` int(11) NOT NULL,
  `urlBoucher` varchar(250) NOT NULL,
  `proceso_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `solicitudjurado`
--

INSERT INTO `solicitudjurado` (`id`, `urlBoucher`, `proceso_id`) VALUES
(1, '020140228E/escanear0003.jpg', 2),
(2, '020140228C/escanear0003.jpg', 6),
(3, '020140228A/escanear0001.jpg', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudoficializacion`
--

CREATE TABLE `solicitudoficializacion` (
  `id` int(11) NOT NULL,
  `tesis` varchar(250) NOT NULL,
  `anteproyecto` varchar(250) NOT NULL,
  `proceso_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `solicitudoficializacion`
--

INSERT INTO `solicitudoficializacion` (`id`, `tesis`, `anteproyecto`, `proceso_id`) VALUES
(1, 'Titulo de la tesis', 'Engineering a compiler.pdf', 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tesis`
--

CREATE TABLE `tesis` (
  `id` int(11) NOT NULL,
  `alumno_codigo` char(10) NOT NULL,
  `horario_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tesis`
--

INSERT INTO `tesis` (`id`, `alumno_codigo`, `horario_id`) VALUES
(1, '020140228E', 1),
(2, '020140228C', 3),
(3, '020140228A', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoproceso`
--

CREATE TABLE `tipoproceso` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipoproceso`
--

INSERT INTO `tipoproceso` (`id`, `descripcion`) VALUES
(1, 'Solicitud de Asesor'),
(2, 'Solicitud de Jurado'),
(3, 'Solicitud de Oficialización'),
(4, 'Solicitud de Sustentación');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `login` varchar(15) NOT NULL,
  `password` varchar(100) NOT NULL,
  `tipo_usuario` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `login`, `password`, `tipo_usuario`) VALUES
(1, 'admin', '$2y$10$T6SqYnBwijofo/PiOLWDxeQb34i8vXaK90QYOlNf7vduUdoXfEbSC', 'S'),
(2, '020140228E', '$2y$10$T6SqYnBwijofo/PiOLWDxeQb34i8vXaK90QYOlNf7vduUdoXfEbSC', 'A'),
(3, '020140228A', '$2y$10$T6SqYnBwijofo/PiOLWDxeQb34i8vXaK90QYOlNf7vduUdoXfEbSC', 'A'),
(4, '020140228C', '$2y$10$T6SqYnBwijofo/PiOLWDxeQb34i8vXaK90QYOlNf7vduUdoXfEbSC', 'A'),
(9, '020140225C', '$2y$10$jux/Gdw8n1YeamCfygh3zeL9gvzAr0oE2.Rj7/e25JGtCdAZhwDLy', 'A');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrativo`
--
ALTER TABLE `administrativo`
  ADD PRIMARY KEY (`id`,`persona_id`),
  ADD KEY `fk_Administrativo_Usuario1_idx` (`usuario_id`),
  ADD KEY `fk_Administrativo_Persona1_idx` (`persona_id`);

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`codigo`,`persona_id`),
  ADD UNIQUE KEY `codigo_UNIQUE` (`codigo`),
  ADD KEY `fk_Alumno_Usuario_idx` (`usuario_id`),
  ADD KEY `fk_Alumno_Escuela1_idx` (`escuela_codigo`),
  ADD KEY `fk_Alumno_Persona1_idx` (`persona_id`);

--
-- Indices de la tabla `docente`
--
ALTER TABLE `docente`
  ADD PRIMARY KEY (`codigo`,`persona_id`),
  ADD KEY `fk_Docente_Persona1_idx` (`persona_id`);

--
-- Indices de la tabla `escuela`
--
ALTER TABLE `escuela`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `funcionjurado`
--
ALTER TABLE `funcionjurado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `jurado`
--
ALTER TABLE `jurado`
  ADD PRIMARY KEY (`docente_codigo`,`tesis_id`),
  ADD KEY `fk_Jurado_FuncionJurado1_idx` (`funcion_id`),
  ADD KEY `fk_Jurado_Tesis1_idx` (`tesis_id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Proceso_Tesis1_idx` (`tesis_id`),
  ADD KEY `fk_Proceso_TipoProceso1_idx` (`tipoproceso_id`);

--
-- Indices de la tabla `solicitudasesor`
--
ALTER TABLE `solicitudasesor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_SolicitudAsesor_Proceso1_idx` (`proceso_id`);

--
-- Indices de la tabla `solicitudjurado`
--
ALTER TABLE `solicitudjurado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_SolicitudJurado_Proceso1_idx` (`proceso_id`);

--
-- Indices de la tabla `solicitudoficializacion`
--
ALTER TABLE `solicitudoficializacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_SolicitudOficializacion_Proceso1_idx` (`proceso_id`);

--
-- Indices de la tabla `tesis`
--
ALTER TABLE `tesis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Tesis_Horario1_idx` (`horario_id`),
  ADD KEY `fk_Tesis_Alumno1_idx` (`alumno_codigo`);

--
-- Indices de la tabla `tipoproceso`
--
ALTER TABLE `tipoproceso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrativo`
--
ALTER TABLE `administrativo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `funcionjurado`
--
ALTER TABLE `funcionjurado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `horario`
--
ALTER TABLE `horario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `proceso`
--
ALTER TABLE `proceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `solicitudasesor`
--
ALTER TABLE `solicitudasesor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `solicitudjurado`
--
ALTER TABLE `solicitudjurado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `solicitudoficializacion`
--
ALTER TABLE `solicitudoficializacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tesis`
--
ALTER TABLE `tesis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipoproceso`
--
ALTER TABLE `tipoproceso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `administrativo`
--
ALTER TABLE `administrativo`
  ADD CONSTRAINT `fk_Administrativo_Persona1` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Administrativo_Usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD CONSTRAINT `fk_Alumno_Escuela1` FOREIGN KEY (`escuela_codigo`) REFERENCES `escuela` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Alumno_Persona1` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Alumno_Usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `docente`
--
ALTER TABLE `docente`
  ADD CONSTRAINT `fk_Docente_Persona1` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `jurado`
--
ALTER TABLE `jurado`
  ADD CONSTRAINT `fk_Jurado_Docente1` FOREIGN KEY (`docente_codigo`) REFERENCES `docente` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Jurado_FuncionJurado1` FOREIGN KEY (`funcion_id`) REFERENCES `funcionjurado` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Jurado_Tesis1` FOREIGN KEY (`tesis_id`) REFERENCES `tesis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proceso`
--
ALTER TABLE `proceso`
  ADD CONSTRAINT `fk_Proceso_Tesis1` FOREIGN KEY (`tesis_id`) REFERENCES `tesis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Proceso_TipoProceso1` FOREIGN KEY (`tipoproceso_id`) REFERENCES `tipoproceso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `solicitudasesor`
--
ALTER TABLE `solicitudasesor`
  ADD CONSTRAINT `fk_SolicitudAsesor_Proceso1` FOREIGN KEY (`proceso_id`) REFERENCES `proceso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `solicitudjurado`
--
ALTER TABLE `solicitudjurado`
  ADD CONSTRAINT `fk_SolicitudJurado_Proceso1` FOREIGN KEY (`proceso_id`) REFERENCES `proceso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `solicitudoficializacion`
--
ALTER TABLE `solicitudoficializacion`
  ADD CONSTRAINT `fk_SolicitudOficializacion_Proceso1` FOREIGN KEY (`proceso_id`) REFERENCES `proceso` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tesis`
--
ALTER TABLE `tesis`
  ADD CONSTRAINT `fk_Tesis_Alumno1` FOREIGN KEY (`alumno_codigo`) REFERENCES `alumno` (`codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Tesis_Horario1` FOREIGN KEY (`horario_id`) REFERENCES `horario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
